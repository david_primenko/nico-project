<?php
/*
$hora = date('H:i:s');

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "registro";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Escape user inputs for security
$cantidad = mysqli_real_escape_string($conn, $_REQUEST['cantidad']);

// attempt insert query execution
$sql = "INSERT INTO peticiones (lugar, producto, cantidad, hora, estado, usuario, fecha) VALUES ('Aqui', 'Whopper', '$cantidad', '$hora', 'Pendiente', 'Joan', CURDATE())";
if ($conn->query($sql) === TRUE) {
    echo "&nbsp&nbsp&nbspPedido realizado!!";
} else {
    echo "Error!!!";
}

$conn->close(); */

?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../css/estilo_menu.css">

</head>
<body>

<!--  <img src="../peticiones/ffondo.jpg" id="fondo"/>

<p>Hola: nombre_cliente</p>  -->

<div class="tab">
    <a href="../menu.php"><button class="tablinks" onclick="openCity(event, 'Inicio')">INICIO</button></a>
    <a href="../hamburguesas/hamburguesas.php"><button class="tablinks" onclick="openCity(event, 'Hamburguesas')">HAMBURGUESAS</button></a>
    <button class="tablinks" onclick="openCity(event, 'Complementos')">COMPLEMENTOS</button>
    <button class="tablinks" onclick="openCity(event, 'Bebidas')">BEBIDAS</button>
    <button class="tablinks" onclick="openCity(event, 'Postres')">POSTRES</button>
</div>

<div class="tabcontent">
<table id="Complementos">

    <tr>
        <td id="titulo"></td>
        <td id="titulo"><h3>COMPLEMENTOS</h3></td>
        <td id="titulo"></td>
    </tr>

    <tr>
        <td id="celda">

            <p id="subtitulo">Patatas fritas</p>
            <p><img src="complementos1.png" height="140" width="200" class="borderimg"></p>
            <p id="valor">1,75€</p>

            <form method="post" action="">
                Cantidad:
                <input type="number" name=cantidad" min="1" max="20">

                <input class="buttonagregar" type="submit" value="Agregar"
            </form>

            <div id="descripcion">
            <p>Patatas fritas saladas con ketchup o mayonesa adaptado a tu sabor mediante nutrientes y alimentos favorables. Sin gluten ni estupefacientes
            que pueden provocar daños al personal. Disfrutarás de su deliciosa salsa palmesana.</p>
            </div>

        </td>

        <td id="celda">

            <p id="subtitulo">Nuggets</p>
            <p><img src="complementos2.png" height="140" width="200" class="borderimg"></p>
            <p id="valor">3,50€</p>

            <form method="post" action="">
                Cantidad:
                <input type="number" name=cantidad" min="1" max="20">

                <input class="buttonagregar" type="submit" value="Agregar"
            </form>

            <div id="descripcion">
            <p>Nuggets deliciosas con o sin salsa de barbacoa. Sin gluten ni estupefacientes
                que pueden provocar daños al personal. Disfrutarás de su deliciosa salsa palmesana.</p>
            </div>

        </td>

        <td id="celda">

            <p id="subtitulo">Alitas de pollos</p>
            <p><img src="complementos3.png" height="140" width="200" class="borderimg"></p>
            <p id="valor">4,00€</p>

            <form method="post" action="">
                Cantidad:
                <input type="number" name=cantidad" min="1" max="20">

                <input class="buttonagregar" type="submit" value="Agregar"
            </form>

            <div id="descripcion">
            <p>Alitas de pollo sabrosas y crujientes. Sin gluten ni estupefacientes
                que pueden provocar daños al personal. Disfrutarás de su deliciosa salsa palmesana.</p>
            </div>
        </td>

    </tr>



</table>
</div>

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>

</body>
</html>

