<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/estilo_menu.css">

</head>
<body>

<!--  <img src="../peticiones/fondo.jpg" id="fondo"/>

<p>Hola: nombre_usuario</p>  -->

<div class="tab">
    <button class="tablinks" onclick="openCity(event, 'Inicio')" id="defaultOpen">INICIO</button>
    <a href="hamburguesas/hamburguesas.php"><button class="tablinks" onclick="openCity(event, 'Hamburguesas')">HAMBURGUESAS</button></a>
    <a href="complementos/complementos.php"><button class="tablinks" onclick="openCity(event, 'Complementos')">COMPLEMENTOS</button></a>
    <button class="tablinks" onclick="openCity(event, 'Bebidas')">BEBIDAS</button>
    <button class="tablinks" onclick="openCity(event, 'Postres')">POSTRES</button>
</div>

<div id="Inicio" class="tabcontent">
    <h3>Inicio</h3>
    <p>Inicio!</p>
</div>

<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>

</body>
</html>




