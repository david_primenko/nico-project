<?php
session_start();
if (!isset($_SESSION['s_username'])) {
    header ("Location: ../login.php");
}else {
    ?>

    <!DOCTYPE HTML>
    <html>

    <head>
        <title>Monitor</title>
        <meta lang="es"
        <meta http-equiv="content-type" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="../../css/estilo_monitor.css">
        <link rel="shortcut icon" href="#" type="image/png">

        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    </head>

    <body>

    <div id="mySidenave" class="sidenave">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="javascript:location.reload()" title="Recargar página" class="recargar"><i class="fas fa-sync-alt"></i></a>
        <a href="monitor_admin.php" style="color: deepskyblue;" title="Tablero de peticiones"><b><i class="fas fa-desktop" style="color: deepskyblue"></i> Tablero</b></a>
        <a href="monitor_grafica.php"><i class="fas fa-chart-pie"></i> Gráfica</a>
        <a href="#"><i class="fas fa-users-cog"></i> Usuarios</a>
        <a href="#"><i class="fas fa-history"></i>&nbsp Logs</a>
        <br>

        <button class="dropdown-btn"><i class="fas fa-user-circle"></i> <?php echo $_SESSION['s_name'];?>
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-container">
            <a id="myBtn" style="font-size: 18px" title="Información perfil"><i class="fas fa-info-circle"></i> Info</a>

            <!-- The Modal -->
            <div id="myModal" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <table>
                        <tr class="tdsinfondoinfo">
                            <td class="tdsinfondoinfo">Nombre:</td>
                            <td class="tdsinfondoinfo"><?php echo "<b>".$_SESSION['s_name']."</b>";?></td>
                        </tr>
                        <tr class="tdsinfondoinfo">
                            <td class="tdsinfondoinfo">Apellidos:</td>
                            <td class="tdsinfondoinfo"><?php echo "<b>".$_SESSION['s_surnames']."</b>";?></td>
                        </tr>
                        <tr class="tdsinfondoinfo">
                            <td class="tdsinfondoinfo">Usuario:</td>
                            <td class="tdsinfondoinfo"><b>Administrador</b></td>
                        </tr>


                    </table>
                </div>

            </div>
            <a href="../logout.php"><i class="fas fa-sign-out-alt"></i> Salir</a>
        </div>

        <hr/>

        <div id="mySidenav" class="sidenav">
            <a id="pendpago">
                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT count(id) total FROM peticiones where estado = '1. Pendiente pago';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row

                    echo "<table border='0' style='width: auto'>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr class='tdsinfondo'>";
                        echo "<td class='tdsinfondo'><font-size='16'>Pend. de pago: " . $row["total"] . "</font></td>";
                        echo "</tr>";

                    }

                    echo "</table>";

                } else {
                    echo "0";
                }
                echo "</table>";
                $conn->close();
                ?>

            </a>
            <a id="cancelado">
                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT count(id) total FROM peticiones where estado = '2. Cancelado';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row

                    echo "<table border='0' style='width: auto'>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr class='tdsinfondo'>";
                        echo "<td class='tdsinfondo'><font-size='16'>Cancelado: " . $row["total"] . "</font></td>";
                        echo "</tr>";

                    }

                    echo "</table>";

                } else {
                    echo "0";
                }
                echo "</table>";
                $conn->close();
                ?>
            </a>
            <a id="pendentrega">
                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT count(id) total FROM peticiones where estado = '3. Pendiente entrega';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row

                    echo "<table border='0' style='width: auto'>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr class='tdsinfondo'>";
                        echo "<td class='tdsinfondo'>Pend. entrega: <b>" . $row["total"] . "</b></td>";
                        echo "</tr>";

                    }

                    echo "</table>";

                } else {
                    echo "0";
                }
                echo "</table>";
                $conn->close();
                ?>
            </a>
            <a id="entregado">
                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT count(id) total FROM peticiones where estado = '4. Entregado';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row

                    echo "<table border='0' style='width: auto'>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr class='tdsinfondo'>";
                        echo "<td class='tdsinfondo'>Entregado: <b>" . $row["total"] . "</b></td>";
                        echo "</tr>";

                    }

                    echo "</table>";

                } else {
                    echo "0";
                }
                echo "</table>";
                $conn->close();
                ?>
            </a>
            <a id="noentregado">
                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT count(id) total FROM peticiones where estado = '5. No entregado';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row

                    echo "<table border='0' style='width: auto'>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr class='tdsinfondo'>";
                        echo "<td class='tdsinfondo'>No entregado: <b>" . $row["total"] . "</b></td>";
                        echo "</tr>";

                    }

                    echo "</table>";

                } else {
                    echo "0";
                }
                echo "</table>";
                $conn->close();
                ?>
            </a>
        </div>
    </div>

    <!-- COMENÇA CONTINGUT -->
    <div id="main">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a href="javascript:location.reload()" title="Recargar página" style="color:black; float: right;"><i class="fas fa-sync-alt"></i></a>

        <div id="bodytitulo">
            <div id="izquierdatitulo"></div>

            <div id="centrotitulo"><h3>Gráfica</h3></div>

            <div id="derechatitulo"></div>
        </div>

        <hr/>

        <div id="bodysubtitulo">
            <div id="izquierdasubtitulo"><h4></h4></div>

            <div id="centrosubtitulo">&nbsp</div>

            <div id="derechasubtitulo"><h4></h4></div>
        </div>



        <div id="body">

            <div id="chart_container">

                <script type="text/javascript">
                    function drawChart() {
                        // call ajax function to get sports data
                        var jsonData = $.ajax({
                            url: "diseño_grafica.php",
                            dataType: "json",
                            async: false
                        }).responseText;
                        //The DataTable object is used to hold the data passed into a visualization.
                        var data = new google.visualization.DataTable(jsonData);

                        // To render the pie chart.
                        var chart = new google.visualization.PieChart(document.getElementById('chart_container'));
                        chart.draw(data, {width: 868, height: 500, colors:["#dc3912", "#109618", "#990099","#ff9900","#3366cc"]});
                    }
                    // load the visualization api
                    google.charts.load('current', {'packages':['corechart']});

                    // Set a callback to run when the Google Visualization API is loaded.
                    google.charts.setOnLoadCallback(drawChart);
                </script>


            </div>


            <div id="centro">&nbsp</div>

            <div id="derecha">

                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT id, lugar, producto, fecha, fecha_resolucion, total, estado FROM peticiones where estado = '4. Entregado';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row
                    echo "<table border='0' style='width: 850px'> <th>ID</th><th>LUGAR<th>PRODUCTO</th><th>FECHA</th><th>FECHA RESOLUCION</th><th>TOTAL (€)</th><th>ESTADO</th>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr>";
                        echo "<td><font-size='40'>" . $row["id"] . "</font></td>";
                        // echo "<td><font-size='40'>" . $row["consumidor"] . "</font></td>";
                        echo "<td><font-size='40'>" . $row["lugar"] . "</font></td>";
                        echo nl2br("<td class='productos'><font-size='40'>" . $row["producto"] . "</font></td>");
                        echo "<td><font-size='40'>" . $row["fecha"] . "</font></td>";
                        echo "<td><font-size='40'>" . $row["fecha_resolucion"] . "</font></td>";
                        echo "<td><font-size='40'>" . $row["total"] . "</font></td>";
                        echo "<td class='entregado'><span class='text2'>Entregado</font></td>";
                        echo "</font></td>";
                        echo "</tr>";

                    }

                    echo "</table>";
                } else {
                    echo "No hay peticiones entregadas";
                }
                echo "</table>";
                $conn->close();
                ?>

            </div>

        </div>


    </div>


            <script>
                // Get the modal
                var modal = document.getElementById('myModal');

                // Get the button that opens the modal
                var btn = document.getElementById("myBtn");

                // Get the <span> element that closes the modal
                var span = document.getElementsByClassName("close")[0];

                // When the user clicks the button, open the modal
                btn.onclick = function() {
                    modal.style.display = "block";
                }

                // When the user clicks on <span> (x), close the modal
                span.onclick = function() {
                    modal.style.display = "none";
                }

                // When the user clicks anywhere outside of the modal, close it
                window.onclick = function(event) {
                    if (event.target == modal) {
                        modal.style.display = "none";
                    }
                }
            </script>



            <script>
                function myFunction() {
                    var x = document.getElementById("myTopnav");
                    if (x.className === "topnav") {
                        x.className += " responsive";
                    } else {
                        x.className = "topnav";
                    }
                }

                // Get the element with id="defaultOpen" and click on it
                document.getElementById("defaultOpen").click();

            </script>


            <script>
                function openNav() {
                    document.getElementById("mySidenave").style.width = "230px";
                    document.getElementById("main").style.marginLeft = "230px";
                }

                function closeNav() {
                    document.getElementById("mySidenave").style.width = "0";
                    document.getElementById("main").style.marginLeft= "0";
                }
            </script>

            <script>
                /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
                var dropdown = document.getElementsByClassName("dropdown-btn");
                var i;

                for (i = 0; i < dropdown.length; i++) {
                    dropdown[i].addEventListener("click", function() {
                        this.classList.toggle("active");
                        var dropdownContent = this.nextElementSibling;
                        if (dropdownContent.style.display === "block") {
                            dropdownContent.style.display = "none";
                        } else {
                            dropdownContent.style.display = "block";
                        }
                    });
                }
            </script>


    </body>
    </html>

<?php } ?>



