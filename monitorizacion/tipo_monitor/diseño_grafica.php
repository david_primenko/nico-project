<?php

require_once __DIR__."/../../src/Controladores/PeticionesControlador.php";

$controlador = new PeticionesControlador();

$data = $controlador->getDataGrafica();
$dataFormateado = [];

foreach($data as $key => $item)
{
    $estado = $item['estado'];
    $total = $item['total'];
    $dataFormateado['cols'][] = array('type' => 'string');
    $dataFormateado['rows'][] = array('c' => array( array('v'=> $estado), array('v'=>(int)$total)) );
}
echo json_encode($dataFormateado)
?>