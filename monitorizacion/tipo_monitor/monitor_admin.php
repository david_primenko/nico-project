<?php
session_start();
if (!isset($_SESSION['s_username'])) {
    header ("Location: ../login.php");
}else {
    ?>

    <!DOCTYPE HTML>
    <html>

    <head>
        <title>Monitor</title>
        <meta lang="es"
        <meta http-equiv="content-type" content="text/html" charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" href="../../css/estilo_monitor.css">
        <link rel="shortcut icon" href="#" type="image/png">

    </head>

    <body>

    <div id="mySidenave" class="sidenave">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="javascript:location.reload()" title="Recargar página" class="recargar"><i class="fas fa-sync-alt"></i></a>
        <a id="defaultOpen" style="color: deepskyblue;" title="Tablero de peticiones"><b><i class="fas fa-desktop" style="color: deepskyblue"></i> Tablero</b></a>
        <a href="monitor_grafica.php"><i class="fas fa-chart-pie"></i> Gráfica</a>
        <a href="#"><i class="fas fa-users-cog"></i> Usuarios</a>
        <a href="#"><i class="fas fa-history"></i>&nbsp Logs</a>
        <br>

        <button class="dropdown-btn"><i class="fas fa-user-circle"></i> <?php echo $_SESSION['s_name'];?>
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-container">
            <a id="myBtn" style="font-size: 18px" title="Información perfil"><i class="fas fa-info-circle"></i> Info</a>

            <!-- The Modal -->
            <div id="myModal" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <table>
                        <tr class="tdsinfondoinfo">
                            <td class="tdsinfondoinfo">Nombre:</td>
                            <td class="tdsinfondoinfo"><?php echo "<b>".$_SESSION['s_name']."</b>";?></td>
                        </tr>
                        <tr class="tdsinfondoinfo">
                            <td class="tdsinfondoinfo">Apellidos:</td>
                            <td class="tdsinfondoinfo"><?php echo "<b>".$_SESSION['s_surnames']."</b>";?></td>
                        </tr>
                        <tr class="tdsinfondoinfo">
                            <td class="tdsinfondoinfo">Usuario:</td>
                            <td class="tdsinfondoinfo"><b>Administrador</b></td>
                        </tr>


                    </table>
                </div>

            </div>
            <a href="../logout.php"><i class="fas fa-sign-out-alt"></i> Salir</a>
        </div>

        <hr/>

        <div id="mySidenav" class="sidenav">
            <?php
            require_once __DIR__."/../../src/Controladores/PeticionesControlador.php";

            $peControlador = new PeticionesControlador();
            $peControlador->addPeticion();

            $estados = $peControlador->getDataGrafica();

            if (count($estados) > 0) {
                foreach ($estados as $estado) {
                    echo "<a id='est-".$estado["id"]."'>";
                    echo "<table border='0' style='width: auto'>";
                    echo "<tr class='tdsinfondo'>";
                    echo "<td class='tdsinfondo'><font-size='16'>".str_replace("Pendiente", "Pend.", $estado["estado"]).": " . $estado["total"] . "</font></td>";
                    echo "</tr>";
                    echo "</table>";
                    echo "</a>";
                }

            }
            ?>
        </div>
    </div>

    <!-- COMENÇA CONTINGUT -->

    <div id="main">
        <span style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
        <a href="javascript:location.reload()" title="Recargar página" style="color:black; float: right;"><i class="fas fa-sync-alt"></i></a>

        <div id="bodytitulo">
            <div id="izquierdatitulo"></div>

            <div id="centrotitulo"><h3>Tablero de peticiones</h3></div>

            <div id="derechatitulo"></div>
        </div>

        <hr/>

        <div id="bodysubtitulo">
            <div id="izquierdasubtitulo"><h4>Peticiones pendiente de pago</h4></div>

            <div id="centrosubtitulo">&nbsp</div>

            <div id="derechasubtitulo"><h4>Peticiones a entregar</h4></div>
        </div>

        <!-- Codigo acciones - actualizaciones -->

        <?php

        if (isset($_POST["pagado"])) {

            $id = $_POST["id"];
            $estado1 = $_POST["estado1"];
            include('../../conexion.php');

// Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }



            $sql = "UPDATE peticiones SET estado = '$estado1' WHERE id = '$id' and estado = '1. Pendiente pago'";

            if ($conn->query($sql) === TRUE) {

            } else {
                echo "<script type='text/javascript'>alert('¡Error ID!');</script>";
            }
            $conn->close();
        }else{

            if (isset($_POST["entregado"])) {

                $id = $_POST["id"];
                $estado2 = $_POST["estado2"];
                include('../../conexion.php');


// Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }


                $sql = "UPDATE peticiones SET estado = '$estado2' WHERE id = '$id' and estado = '3. Pendiente entrega'";

                if ($conn->query($sql) === TRUE) {

                } else {
                    echo "<script type='text/javascript'>alert('¡Error ID!');</script>";
                }
                $conn->close();
            }
        }
        ?>


        <div id="body">

            <div id="izquierda">

                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }
                $sql = "SELECT id, lugar, producto, fecha, total, estado FROM peticiones where estado = '1. Pendiente pago';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row

                    echo "<table border='0' style='width: 850px'> <th>ID</th><th>LUGAR<th>PRODUCTO</th><th>FECHA</th><th>TOTAL (€)</th><th>ESTADO</th>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr>";
                        echo "<td><font-size='40'>" . $row["id"] . "</font></td>";
                        // echo "<td><font-size='40'>" . $row["consumidor"] . "</font></td>";
                        echo "<td><font-size='40'>" . $row["lugar"] . "</font></td>";
                        echo nl2br("<td class='productos'><font-size='40'>" . $row["producto"] . "</font></td>");
                        echo "<td><font-size='40'>" . $row["fecha"] . "</font></td>";
                        echo "<td><font-size='40'>" . $row["total"] . "</font></td>";
                        echo "<td class='pendientepago'><span class='parpadea text2'>Pendiente pago</font></td>";

                        echo "</font></td>";
                        echo "</tr>";

                    }

                    echo "</table>";

                } else {
                    echo "No hay peticiones pendientes de pago";
                }
                echo "</table>";
                $conn->close();
                ?>

            </div>

            <div id="centro">&nbsp</div>

            <div id="derecha">

                <?php
                include('../../conexion.php');

                // Create connection
                $conn = new mysqli($servername, $username, $password,$dbname);

                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT id, lugar, producto, fecha, total, estado FROM peticiones where estado = '3. Pendiente entrega';";
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    // output data of each row
                    echo "<table border='0' style='width: 850px'> <th>ID</th><th>LUGAR<th>PRODUCTO</th><th>FECHA</th><th>TOTAL (€)</th><th>ESTADO</th>";

                    while($row = $result->fetch_assoc())
                    {

                        echo "<tr>";
                        echo "<td><font-size='40'>" . $row["id"] . "</font></td>";
                        // echo "<td><font-size='40'>" . $row["consumidor"] . "</font></td>";
                        echo "<td><font-size='40'>" . $row["lugar"] . "</font></td>";
                        echo nl2br("<td class='productos'><font-size='40'>" . $row["producto"] . "</font></td>");
                        echo "<td><font-size='40'>" . $row["fecha"] . "</font></td>";
                        echo "<td><font-size='40'>" . $row["total"] . "</font></td>";
                        echo "<td class='pendienteentrega'><span class='parpadea text2'>Pendiente entrega</font></td>";
                        echo "</font></td>";
                        echo "</tr>";

                    }

                    echo "</table>";
                } else {
                    echo "No hay peticiones pendientes a entregar";
                }
                echo "</table>";
                $conn->close();
                ?>

            </div>

        </div>


        <div id="bodycuadrados">
            <div id="izquierdacuadrado">
                <table style="width: 45%">
                    <tr>
                        <th>
                            ID PEDIDO A PAGAR/CANCELAR
                        </th>
                    </tr>

                    <tr>
                        <td>

                            <form method='post' action=''>
                                <input list="id" name="id" placeholder="Selecciona ID">
                                <datalist id="id">
                                    <?php
                                    include('../../conexion.php');

                                    // Create connection
                                    $conn = new mysqli($servername, $username, $password,$dbname);

                                    // Check connection
                                    if ($conn->connect_error) {
                                        die("Connection failed: " . $conn->connect_error);
                                    }

                                    $sql = "SELECT id FROM peticiones where estado = '1. Pendiente pago'";
                                    $result = $conn->query($sql);

                                    while ($row = $result->fetch_assoc()) {

                                        // echo '<option value="'.$row['id'].'">'.$row['id'].'</option>';
                                        echo "<option value='".$row['id']."'>".$row['id']."</option>";

                                    }
                                    ?>
                                </datalist>
                                <select style="width:auto" name="estado1">
                                    <option name="estado1" value="3. Pendiente entrega" selected>Pagado</option>
                                    <option name="estado1" value="2. Cancelado">Cancelado</option>
                                </select>
                                <input class="buttonagregar" type="submit" name="pagado" value="Listo">
                            </form>

                        </td>
                    </tr>
                </table>
            </div>

            <div id="centrocuadrado">&nbsp</div>

            <div id="derechacuadrado">

                <table style="width: 45%">
                    <tr>
                        <th>
                            ID PEDIDO A ENTREGAR/NO ENTREGAR
                        </th>
                    </tr>

                    <tr>
                        <td>
                            <form method='post' action=''>
                                <input list="id" name="id" placeholder="Selecciona ID">
                                <datalist id="id">
                                    <?php
                                    include('../../conexion.php');

                                    // Create connection
                                    $conn = new mysqli($servername, $username, $password,$dbname);

                                    // Check connection
                                    if ($conn->connect_error) {
                                        die("Connection failed: " . $conn->connect_error);
                                    }

                                    $sql = "SELECT id FROM peticiones where estado = '3. Pendiente entrega'";
                                    $result = $conn->query($sql);

                                    while ($row = $result->fetch_assoc()) {

                                        //  echo "<option value='".$row['id']."'>".$row['id']."</option>";
                                        echo "<option value='".$row['id']."'>".$row['id']."</option>";

                                    }
                                    ?>
                                </datalist>
                                <select style="width:auto" name="estado2">
                                    <option name="estado2" value="4. Entregado" selected>Entregado</option>
                                    <option name="estado2" value="5. No entregado">No entregado</option>
                                </select>
                                <input class="buttonagregar" type="submit" name="entregado" value="Listo">
                            </form>
                        </td>
                    </tr>
                </table>
            </div>

        </div>

    </div>


    <script>
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal
        btn.onclick = function() {
            modal.style.display = "block";
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>


    <script>

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();

    </script>


    <script>
        function openNav() {
            document.getElementById("mySidenave").style.width = "230px";
            document.getElementById("main").style.marginLeft = "230px";
        }

        function closeNav() {
            document.getElementById("mySidenave").style.width = "0";
            document.getElementById("main").style.marginLeft= "0";
        }
    </script>

    <script>
        /* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
        var dropdown = document.getElementsByClassName("dropdown-btn");
        var i;

        for (i = 0; i < dropdown.length; i++) {
            dropdown[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var dropdownContent = this.nextElementSibling;
                if (dropdownContent.style.display === "block") {
                    dropdownContent.style.display = "none";
                } else {
                    dropdownContent.style.display = "block";
                }
            });
        }
    </script>


    </body>
    </html>

<?php } ?>



