<?php

session_start();

if ($_POST) {

include('../conexion.php');


    $nombre_usuario = mysqli_real_escape_string($conn, $_POST["nombre_usuario"]);
    $clave = mysqli_real_escape_string($conn, $_POST["clave"]);

mysqli_set_charset($conn, 'utf8');

    // Usuari activat, dades correctes i inici de sessió administrador
    $sql = "select nombre_usuario, nombre, apellidos, clave, nivel_usuario, actividad FROM usuarios_sistema where nombre_usuario = '$nombre_usuario' and clave = '$clave' and nivel_usuario = '1' and actividad = 'S'";
    $result = $conn->query($sql);

    // Usuari activat, dades correctes i inici de sessió usuario regular
    $sql2 = "select nombre_usuario, clave, nivel_usuario, actividad FROM usuarios_sistema where nombre_usuario = '$nombre_usuario' and clave = '$clave' and nivel_usuario = '0' and actividad = 'S'";
    $result2 = $conn->query($sql2);

    // Usuari activat, usuari correcte i pass incorrecta,
    $sql3 = "select nombre_usuario, clave, actividad FROM usuarios_sistema where nombre_usuario = '$nombre_usuario' and clave != '$clave' and actividad = 'S'";
    $result3 = $conn->query($sql3);

    // Dades correctes però usuari desactivat
    $sql4 = "select nombre_usuario, clave, actividad FROM usuarios_sistema where nombre_usuario = '$nombre_usuario' and clave = '$clave' and actividad = 'N'";
    $result4 = $conn->query($sql4);

    // Usuari correcte, pass incorrecte, desactivat
    $sql5 = "select nombre_usuario, clave, actividad FROM usuarios_sistema where nombre_usuario = '$nombre_usuario' and clave != '$clave' and actividad = 'N' ";
    $result5 = $conn->query($sql5);


    if ($result->num_rows > 0) {
// output data of each row
        while ($row = $result->fetch_assoc()) {
            header( "Location: ../monitorizacion/tipo_monitor/monitor_admin.php" );
            $_SESSION["s_username"] = $row['nombre_usuario'];
        $_SESSION["s_name"] = $row['nombre'];
        $_SESSION["s_surnames"] = $row['apellidos'];
    }

    } elseif ($result2->num_rows > 0) {
        while ($row = $result2->fetch_assoc()) {
            header( "Location: ../monitorizacion/tipo_monitor/monitor_regular.php" );
            $_SESSION["s_username"] = $row['nombre_usuario'];
        }

    } elseif ($result3->num_rows > 0) {
        while ($row = $result3->fetch_assoc()) {
            $nopass = 'Contraseña incorrecta';
        }

    } elseif ($result4->num_rows > 0) {
        while ($row = $result4->fetch_assoc()) {
            $noactive2 = 'Los datos son correctos pero usted no está dado/a de alta. Contacte con el administrador para la activación.';

        }

    } elseif ($result5->num_rows > 0) {
        while ($row = $result5->fetch_assoc()) {
            $noactive = 'El nombre de usuario existe pero no está activado y la contraseña es incorrecta. Contacte con el administrador para ello.';
        }

    } else {
        $noexist = 'El nombre de usuario no existe';
    }

    $conn->close();

}?>

<!DOCTYPE HTML>
<html>

<head>
    <title>Login </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/w3.css">
    <link rel="stylesheet" href="../css/estilsin.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
    <link rel="shortcut icon" href="info.png" type="image/png" />
    <meta name="author" content="asds">

</head>
<img src="../peticiones/fondo8.jpg" id="fondo_login">
<body>

<br>
<h1 class="w3-lobster" id="titol">¡Bienvenido!</h1>

<form class="w3-container w3-dark-gray" id="a1" method="POST" action="">

    <p>
        Nombre usuario:
        <input class="w3-input a2" style="width:50%" type="text" name="nombre_usuario" required>
            <span class="error" style="font-weight: bold;"><?php
                if (isset($noexist)){
                    printf($noexist);
                    unset($noexist);
                }
                ?>
            </span>
    </p>
    <p>
        Contraseña:
        <input class="w3-input a2" style="width:50%" type="password" name="clave" required>
        <span class="error" style="font-weight: bold;"><?php
            if (isset($nopass)){
                printf($nopass);
                unset($nopass);
            }
            ?>
        </span>

    </p>

    <div>
        <a class="w3-btn w3-blue w3-border w3-border-white w3-round-xlarge a1" href="../registro/registro.php">Regístrate</a>
        <input class="w3-btn w3-green w3-border w3-border-white w3-round-xlarge a1" type="submit" name="Enviar" value="Entrar">
    </div>

    <div>
        <h6>
            <a href="#">¿Has olvidado los datos de la cuenta?</a>  <!-- Encara falta això  -->
        </h6>
    </div>

    <div>
    <h5><span class="error"><?php
        if (isset($noactive)){
            printf($noactive);
            unset($noactive);
        }
        ?>
    </h5>

     <h5><span class="error"><?php
                    if (isset($noactive2)){
                        printf($noactive2);
                        unset($noactive2);
                    }
                    ?>
    </h5>
    </div>

</form>

</body>

</html>