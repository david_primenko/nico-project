<?php

include('../conexion.php');

if ($_POST)
{
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$nombre_usuario = mysqli_real_escape_string($conn,$_POST["nombre_usuario"]);
$nombre = mysqli_real_escape_string($conn,$_POST["nombre"]);
$apellidos = mysqli_real_escape_string($conn,$_POST["apellidos"]);
$clave = mysqli_real_escape_string($conn,$_POST["clave"]);
// $clave=md5("clave");

$sql = "select nombre_usuario FROM usuarios_sistema where nombre_usuario = '$nombre_usuario'";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
// output data of each row
    while($row = $result->fetch_assoc()) {
        $noexist='Este nombre de usuario ya existe';
    }
} else {
    $sql = "INSERT INTO usuarios_sistema (nombre_usuario, nombre, apellidos, clave) VALUES ('$nombre_usuario', '$nombre', '$apellidos', '$clave')";

    if ($conn->query($sql) === TRUE) {

        echo "<script type='text/javascript'>alert('¡Te has registrado correctamente! El último paso sería contactar con el administrador para que le active la cuenta. ¡GRACIAS!');
        window.location='../monitorizacion/login.php';</script>";


    } else {
        echo "<script type='text/javascript'>alert('¡Parece haber habido un problema, vuelve a intentarlo!');
        window.location='./registro.php';</script>";

    }
}

}$conn->close();
?>

<!DOCTYPE HTML>
<html>

<head>
    <title>Registro</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../css/w3.css">
    <link rel="stylesheet" href="../css/estilsin.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster">
    <meta name="author" content="asdf">
</head>
<img src="../peticiones/fondo5.jpg" id="fondo_reg">
<body>
<h1 class="w3-lobster" id="titol2">Registro</h1>
<form class="w3-container w3-dark-gray" id="a1" method="POST" action="">
    <p>
        Nombre usuario:
        <input class="w3-input a2" style="width:50%" type="text" name="nombre_usuario" required>

    <span class="error"><?php
        if (isset($noexist)){
            printf($noexist);
            unset($noexist);
        }
        ?>
    </span></p>
    <p>
        Nombre:
        <input class="w3-input a2" style="width:50%" type="text" name="nombre" required>
    </p>
    <p>
        Apellidos:
        <input class="w3-input a2" style="width:50%" type="text" name="apellidos" required>
    </p>
    <p>
        Contraseña:
        <input class="w3-input a2" style="width:50%" type="password" name="clave" pattern="[A-Za-z0-9@#$%]{8,20}" title="Una contraseña válida es una cadena con una longitud entre 8 y 20 caracteres, donde cada uno consiste en una letra mayúscula o minúscula, un dígito, o los símboloss '@', '#', '$' y '%'" required>
    </p>

    <br><br>
    <input class="w3-btn w3-green w3-border w3-border-white w3-round-xlarge a3" type="submit" name="Enviar" value="Registrar">&nbsp;
    <input class="w3-btn w3-red w3-border w3-border-white w3-round-xlarge a3" type="reset" value="Borrar campos" name="B2">&nbsp;
    <a class="w3-btn w3-white w3-border w3-border-black w3-round-xlarge a4" href="../monitorizacion/login.php">Atrás</a>
</form>

</body>

</html>
