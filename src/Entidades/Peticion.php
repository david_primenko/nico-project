<?php

require_once __DIR__."/Base.php";

class Peticion extends Base {
    const TABLE_NAME = "peticiones";
    protected $id;
    protected $id_tipo_peticion;
    protected $id_estado;
    protected $fecha;
    protected $fecha_resolucion;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdTipoPeticion()
    {
        return $this->id_tipo_peticion;
    }

    /**
     * @param mixed $id_tipo_peticion
     */
    public function setIdTipoPeticion($id_tipo_peticion)
    {
        $this->id_tipo_peticion = $id_tipo_peticion;
    }

    /**
     * @return mixed
     */
    public function getIdEstado()
    {
        return $this->id_estado;
    }

    /**
     * @param mixed $id_estado
     */
    public function setIdEstado($id_estado)
    {
        $this->id_estado = $id_estado;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getFechaResolucion()
    {
        return $this->fecha_resolucion;
    }

    /**
     * @param mixed $fecha_resolucion
     */
    public function setFechaResolucion($fecha_resolucion)
    {
        $this->fecha_resolucion = $fecha_resolucion;
    }



    function getTableName()
    {
        return self::TABLE_NAME;
    }
}