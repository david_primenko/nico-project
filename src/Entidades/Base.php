<?php

abstract class Base {

    abstract function getTableName();

    public function createFromAssoc($data) {
        $class = get_called_class();
        $obj = new $class();
        foreach ($data as $key => $item) {
            $obj[$key] = $item;
        }

        return $item;
    }
}