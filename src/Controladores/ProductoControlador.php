<?php
require_once  __DIR__."/../Conexion.php";
require_once __DIR__."/ControladorAbstracto.php";
require_once __DIR__."/../Entidades/Producto.php";

class ProductoControlador extends ControladorAbstracto
{

    function getClass()
    {
        return Producto::class;
    }
}