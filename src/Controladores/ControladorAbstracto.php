<?php

require_once __DIR__."/../Conexion.php";

abstract class ControladorAbstracto {

    abstract function getClass();

    public function getAll() {
        $conn = Conexion::getInstance();
        return $conn->select($this->getClass()::getTableName());
    }

    public function get() {
        $conn = Conexion::getInstance();
        return $conn->select($this->getClass()::getTableName(), "SELECT * FROM ?;");
    }

    public function crear() {

    }

    public function actualizar() {

    }

    public function borrar() {

    }
}