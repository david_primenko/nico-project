<?php

require_once  __DIR__."/ControladorAbstracto.php";
require_once  __DIR__."/../Entidades/Peticion.php";

class PeticionesControlador extends ControladorAbstracto
{
    function getClass()
    {
        return Peticion::class;
    }

    public function getDataGrafica() {
        $conn = Conexion::getInstance();
        return $conn->select("SELECT count(*) as total, e.id, e.nombre as estado FROM ".Peticion::TABLE_NAME." as ent INNER JOIN estado as e ON ent.id_estado = e.id GROUP BY e.nombre;");
    }

    public function addPeticion() {
        $conn = Conexion::getInstance();
        return $conn->queryExecute("INSERT INTO estado VALUES(null, 'Hola');");
    }
}
