<?php

require_once "ControladorAbstracto.php";
require_once "../Entidades/TipoPeticion.php";

class TipoPeticionControlador extends ControladorAbstracto
{

    function getClass()
    {
        return TipoPeticion::class;
    }
}