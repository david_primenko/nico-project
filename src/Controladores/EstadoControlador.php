<?php

require_once "ControladorAbstracto.php";
require_once "../Entidades/Estado.php";

class EstadoControlador extends ControladorAbstracto
{

    function getClass()
    {
        return Estado::class;
    }
}