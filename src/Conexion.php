<?php
define("DSN", "mysql:dbname=registro;host=127.0.0.1");
define("USERNAME", "root");
define("PASSWORD", "");

class Conexion {

    private static $instance;
    private $conn;

    private function __construct()
    {
        try {
            $this->conn = new PDO(DSN, USERNAME, PASSWORD);
        } catch (PDOException $e){
            exit($e->getMessage());
        }
    }

    public static function getInstance() {
        if (!self::$instance instanceof self) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function select($sql) {
        $stm = $this->conn->query($sql);
        $result = $stm->fetchAll();

        return $result;
    }

    public function queryExecute($sql) {
        $stm = $this->conn->prepare($sql);
        $exec = $stm->execute($stm);

        return $exec;
    }
}